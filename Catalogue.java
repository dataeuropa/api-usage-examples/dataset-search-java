import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Catalogue {

    public static void main(String[] args) throws IOException, InterruptedException {

        // retrieve the information for the catalogue 'EU institutions', which has the ID 'european-union-open-data-portal'
        // https://data.europa.eu/api/hub/search/catalogues/european-union-open-data-portal
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://data.europa.eu/api/hub/search/catalogues/european-union-open-data-portal"))
                .GET()
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
    }
}
