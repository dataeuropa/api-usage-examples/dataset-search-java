# Java examples for data.europa.eu Search API

## Prerequisites

1. Java

## Examples

The examples can be compiled with `javac {filename}`, e.g. `javac Search.java`.
To run the program, `javav {Program}` has to be used, where `{Program}` is the filename without extension, e.g. `javac Search`.
The examples first have to be compiled, before they can be run.

### Search

To search for a term, the Endpoint `/search` has to be used with the parameter `q`.

[Search.java](Search.java):
```java

        // search for the term 'Covid 19'
        //https://data.europa.eu/api/hub/search/search?q=covid 19
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://data.europa.eu/api/hub/search/search?q=covid%2019"))
                .GET()
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

```

### Search in specific catalogue

To limit the search to a specific catalogue, the `facets` query parameter has to be used with the catalog facet.
Additionally, to enable the facet search, the query parameter `filter` has to be set to `datasets`:

[SearchWithFilter.java](SearchWithFilter.java):
```java
import requests
// search for the term 'Covid 19' in the catalogue 'EU institutions', which has the id 'european-union-open-data-portal'
// https://data.europa.eu/api/hub/search/search?q=covid 19&facets={'catalog':['european-union-open-data-portal']}
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://data.europa.eu/api/hub/search/search?q=covid%2019&facets=%7B%22catalog%22%3A%5B%22european-union-open-data-portal%22%5D%7D&filter=dataset"))
                .GET()
                .build();
                HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
```

### Retrieve a specific catalogue

To retrieve the information for a specific catalogue, the endpoint `/catalogues` has to be used with the catalogue ID in the path.

[Catalogue.java](Catalogue.java):
```java
// retrieve the information for the catalogue 'EU institutions', which has the ID 'european-union-open-data-portal'
// https://data.europa.eu/api/hub/search/catalogues/european-union-open-data-portal
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://data.europa.eu/api/hub/search/catalogues/european-union-open-data-portal"))
                .GET()
                .build();
                HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
```

### Retrieve a specific dataset

To retrieve the information for a specific dataset, the endpoint `/datasets` has to be used with the dataset ID in the path.

[Dataset.java](Dataset.java):
```java
// retrieve the information for a datasts in the catalogue 'EU institutions'
// the dataset has the ID 'g3ebxpyzj2lulbjqwkhg'
// https://data.europa.eu/api/hub/search/datasets/g3ebxpyzj2lulbjqwkhg
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://data.europa.eu/api/hub/search/datasets/g3ebxpyzj2lulbjqwkhg"))
                .GET()
                .build();
                HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
```
