import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class SearchWithFilter {

    public static void main(String[] args) throws IOException, InterruptedException {
        // search for the term 'Covid 19' in the catalogue 'EU institutions', which has the id 'european-union-open-data-portal'
        // https://data.europa.eu/api/hub/search/search?q=covid 19&facets={'catalog':['european-union-open-data-portal']}
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://data.europa.eu/api/hub/search/search?q=covid%2019&facets=%7B%22catalog%22%3A%5B%22european-union-open-data-portal%22%5D%7D&filter=dataset"))
                .GET()
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
    }
}
