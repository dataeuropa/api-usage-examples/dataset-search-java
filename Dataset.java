import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Dataset {
    public static void main(String[] args) throws IOException, InterruptedException {
    // retrieve the information for a datasts in the catalogue 'EU institutions'
    // the dataset has the ID 'g3ebxpyzj2lulbjqwkhg'
        // https://data.europa.eu/api/hub/search/datasets/g3ebxpyzj2lulbjqwkhg
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://data.europa.eu/api/hub/search/datasets/g3ebxpyzj2lulbjqwkhg"))
                .GET()
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
    }
}
