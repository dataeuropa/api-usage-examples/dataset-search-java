import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Search {

    public static void main(String[] args) throws IOException, InterruptedException {
        // search for the term 'Covid 19'
        //https://data.europa.eu/api/hub/search/search?q=covid 19
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://data.europa.eu/api/hub/search/search?q=covid%2019"))
                .GET()
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
    }
}
